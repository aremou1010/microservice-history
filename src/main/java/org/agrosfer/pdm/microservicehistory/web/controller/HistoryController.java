/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicehistory.web.controller;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.agrosfer.pdm.microservicehistory.dao.HistoryDAO;
import org.agrosfer.pdm.microservicehistory.model.History;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author g3a
 */
@RestController
public class HistoryController {
    
    @Autowired
    HistoryDAO historyDAO;
    
    @PostMapping("/histories")
    public ResponseEntity<?> create(@RequestBody History history){
        
        history.setCreateAt(Date.from(Instant.now()));
        
        History newHistory = null;
        newHistory = historyDAO.insert(history);
        
        if (newHistory != null) {
            return ResponseEntity.accepted()
                    .body(newHistory);
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error : cannot create history.");
    }
    
    //@GetMapping("/histories")
    public ResponseEntity<?> getAll(){
        
        List<History> allHistory = historyDAO.findAll();
        
        return ResponseEntity.ok()
                    .body(allHistory);
    }
    
    @GetMapping("/histories/user/{userRef}")
    public ResponseEntity<?> getAllByUserReference(@PathVariable("userRef") String userRef){
        
        List<History> allHistory = historyDAO.findByUserReference(userRef);
        
        return ResponseEntity.ok()
                    .body(allHistory);
    }
    
    @GetMapping("/histories/object/{object}")
    public ResponseEntity<?> getAllByObject(@PathVariable("object") String object){
        
        List<History> allHistory = historyDAO.findByObject(object);
        
        return ResponseEntity.ok()
                    .body(allHistory);
    }
    
    @GetMapping("/histories/object_ref/{objectRef}")
    public ResponseEntity<?> getAllByObjectReference(@PathVariable("objectRef") String objectRef){
        
        List<History> allHistory = historyDAO.findByObjectReference(objectRef);
        
        return ResponseEntity.ok()
                    .body(allHistory);
    }
    
}
