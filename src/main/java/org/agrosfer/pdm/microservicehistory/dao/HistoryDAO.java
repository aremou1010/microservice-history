/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicehistory.dao;

import java.util.List;
import org.agrosfer.pdm.microservicehistory.model.History;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author g3a
 */
public interface HistoryDAO extends MongoRepository<History, Long> {
    
    List<History> findByUserReference(String userReference);
    
    List<History> findByObject(String object);
    
    List<History> findByObjectReference(String objectRef);
    
}
