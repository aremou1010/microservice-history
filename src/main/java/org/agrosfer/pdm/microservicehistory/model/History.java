/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicehistory.model;

import java.sql.Timestamp;
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author g3a
 */
@Data
@Document
public class History {
    
    @Id
    private String id;
    
    private String object;
    
    private String action;
    
    private Long objectId;
    
    private String objectReference;
    
    private String attribute;
    
    private String oldValue;
    
    private String newValue;
    
    private Long amount;
    
    private String details;
    
    private String userReference;
    
    private Date createAt;

    public History() {
    }

    public History(String id, String object, String action, Long objectId, String objectReference, String attribute, String oldValue, String newValue, Long amount, String details, String userReference, Date createAt) {
        this.id = id;
        this.object = object;
        this.action = action;
        this.objectId = objectId;
        this.objectReference = objectReference;
        this.attribute = attribute;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.amount = amount;
        this.details = details;
        this.userReference = userReference;
        this.createAt = createAt;
    }
    
}
